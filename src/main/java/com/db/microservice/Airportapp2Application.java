package com.db.microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Airportapp2Application {
	public static void main(String[] args) {
		SpringApplication.run(Airportapp2Application.class, args);
	}

}
