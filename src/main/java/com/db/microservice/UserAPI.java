package com.db.microservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "user")
public class UserAPI {

	@Autowired
	private UserDAO userDAO;

	@PostMapping
	public @ResponseBody User createUser(@RequestBody User user) {
		userDAO.save(user);
		return user;
	}

	@GetMapping(value = "/{id}")
	public @ResponseBody User createUser(@PathVariable Long id) {
		return userDAO.findById(id).get();
	}

}