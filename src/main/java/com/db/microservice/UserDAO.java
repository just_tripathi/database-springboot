package com.db.microservice;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * only INterface
 * 
 * @author 
 *
 */
public interface UserDAO extends JpaRepository<User, Long> {

}